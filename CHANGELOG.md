# CHANGELOG.md

## 0.2.4

### Fixes

- Fixes link to raw_data path.

## 0.2.3

### Fixes

- Fixes the sample and dataset in sending the PROCESSED_DATA to iCAT.

## 0.2.2

### Fixes

- Fix the missing `Sample_name` in icat_metadata

### Changes

- Change the concept of `output` of the Eiger2Crysalis task to return the output path like:
  `/data/visitor/proposal/beamline/session/PROCESSED_DATA/sample/sample_dataset/sample_dataset_scan`
  This helps to provide the right path to all tasks that create files.
- Removes the useless method `get_scans_from_user_parameters`, this is done on the blissoda side now

## 0.2.1

### Added
- Added new task `averageFrames` to average frames from HDF5 files.
- Added new task `dataPortal` for publication to the ESRF data portal (data.esrf.fr):
  - Creates a image of the average of the scan in 8-bit.
  - Applies configurable binning.
  - Saves the processed image as a PNG in a dedicated "gallery" folder.
  - Infers ICAT parameters from the processed data directory and stores processed data metadata using pyicat_plus.
- Updated the demo workflow JSON to include data mappings for the new tasks.

## 0.2.0

### Added
- Implemented new tasks for file processing in the conversion pipeline:
  - `createIniFiles` for handling and copying `.ini` files.
  - `createSetCcdFiles` for processing `.set` and `.ccd` files.
  - `createRunFiles` for creating `.run` files.
  - `createParFiles` for generating updated `.par` files based on input configurations.
- Added a demo ewoks workflow to include these new tasks with detailed data mappings.
- Added a requirement on `cryio`.

## 0.1.2

### Black 25.1 compatibility

Adds compatibility for black 25.1

### README.md

Improves README.md file.

## 0.1.1

### Fix links

Links for pypi have been fixed.

## 0.1.0

### Added
- Implemented `Eiger2Crysalis` task to convert HDF5 files to Esperanto format.
- Integrated `fabio.app.eiger2crysalis.Converter` and `fabio.esperantoimage` for image conversion.
- Defined input parameters for `Eiger2Crysalis`, including `images`, `output`, `distance`, `beam`, `wavelength`, and `polarization`.
- Developed a `pytest` test suite for `Eiger2Crysalis`, ensuring proper handling of HDF5 files and conversion outputs.
- Added test fixtures providing mock input files and configurations.

### Changed
- Updated documentation with task descriptions.

### Fixed
- Ensured compatibility with expected HDF5 file structures.
