ewoksscxrd |version|
=============================

*ewoksscxrd* provides a template for projects that implements Ewoks tasks with Orange widgets.

*ewoksscxrd* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_
of the `European Synchrotron <https://www.esrf.eu/>`_.

Create new project
------------------

To start a new project called `myproject` based on the template, you first clone the template project

.. code::

    git clone https://gitlab.esrf.fr/workflow/ewoksapps/ewoksscxrd.git myproject
    rm -rf myproject/.git

Replace the string `ewoksscxrd` with `myproject` in all your files

.. code::

    grep -rl ewoksscxrd ./myproject | xargs sed -i 's/ewoksscxrd/myproject/g'

Rename all `ewoksscxrd` directories with `myproject`

.. code::

    find ./myproject -depth -name "*ewoksscxrd*" | \
    while IFS= read -r ent; do mv $ent ${ent%ewoksscxrd*}myproject${ent##*ewoksscxrd}; done

Register the Orange widgets in the current python environment and run the tests to check that everything works

.. code::

    pip install -e ./myproject[test]  # installs Ewoks, Orange and test utilities
    pytest ./myproject

To build the documentation of your project

.. code::

    pip install -e ./myproject[doc]  # install doc utilities
    sphinx-build ./myproject/doc ./myproject/build/sphinx/html -E -a
    firefox myproject/build/sphinx/html/index.html

Project structure
-----------------

An Ewoks project with Orange widgets typically has this structure

.. code::

    .
    ├── LICENSE.md
    ├── pyproject.toml
    ├── README.md
    ├── setup.cfg
    ├── setup.py
    └── src
        ├── ewoksscxrd
        │   ├── __init__.py
        │   ├── tasks
        │   │   ├── __init__.py
        │   │   └── sumtask.py
        │   └── tests
        │       ├── conftest.py
        │       ├── __init__.py
        │       ├── test_sumtask.py
        │       └── test_tutorials.py
        └── orangecontrib
            ├── ewoksscxrd
            │   ├── categories  # THIS IS OPTIONAL
            │   │   ├── examples1
            │   │   │   ├── icons
            │   │   │   │   ├── category.svg
            │   │   │   │   ├── __init__.py
            │   │   │   │   └── sum.png
            │   │   │   ├── __init__.py
            │   │   │   ├── sumtask.py
            │   │   │   └── tutorials
            │   │   │       ├── examples1.ows
            │   │   │       └── __init__.py
            │   │   ├── examples2
            │   │   │   ├── icons
            │   │   │   │   ├── category.svg
            │   │   │   │   ├── __init__.py
            │   │   │   │   └── sum.png
            │   │   │   ├── __init__.py
            │   │   │   ├── sumtask.py
            │   │   │   └── tutorials
            │   │   │       ├── examples2.ows
            │   │   │       └── __init__.py
            │   │   └── __init__.py
            │   ├── icons
            │   │   ├── category.svg
            │   │   ├── __init__.py
            │   │   └── sum.png
            │   ├── __init__.py
            │   ├── sumtask.py
            │   └── tutorials
            │       ├── examples.ows
            │       └── __init__.py
            └── __init__.py

The template provides

* example ewoks tasks
* example orange widgets for each Ewoks task (one or more categories)
* tutorial workflows
* unit tests for ewoks tasks, orange widgets and tutorial workflows
* sphinx documentation

The modules that implement Ewoks tasks:

* `src/ewoksscxrd/tasks/sumtask.py`: example of ewoks tasks (pure computation, no GUI)

The modules that implement Orange widgets:

* `src/orangecontrib/ewoksscxrd/sumtask.py`: example of an Orange widget in the main category (see `NAME = "Ewoks Examples"`)
* `src/orangecontrib/ewoksscxrd/categories/examples1/sumtask.py`: example of an Orange widget in the category "Examples1" (see `NAME = "Ewoks Examples (1)"`)
* `src/orangecontrib/ewoksscxrd/categories/examples1/sumtask.py`: example of an Orange widget in the category "Examples2" (see `NAME = "Ewoks Examples (2)"`)

The `[options.entry_points]` section in `setup.cfg` specifies the modules where Orange widgets and tutorials are located.

.. toctree::
    :hidden:

    api
